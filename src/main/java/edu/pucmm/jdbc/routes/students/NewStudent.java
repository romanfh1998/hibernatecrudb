package edu.pucmm.jdbc.routes.students;

import edu.pucmm.jdbc.models.RouteBase;
import edu.pucmm.jdbc.utils.DB;

import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;
import spark.Request;
import spark.Response;

/**
 * @author a.marte
 */
public class NewStudent extends RouteBase {

    public NewStudent() {
        super("/students/create");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        //id
        
        
        String id = request.queryParams("id");
        String names = request.queryParams("names");
        String lastnames = request.queryParams("lastnames");
        String birthStr = request.queryParams("birdth");
       
        
        
        System.out.println("ensenar los valores que esta en java id=" + id +" names = "+ names + " " + lastnames + " " + birthStr);
        if (names != null && lastnames != null && birthStr!= null
                && !names.isEmpty() && !lastnames.isEmpty() && !birthStr.isEmpty()) {
            // TODO el insert puede estar mal
            try{
                
                        SimpleDateFormat format = new SimpleDateFormat("ddMMyyyy");
        Date parsed = format.parse(birthStr);
        java.sql.Date birdth = new java.sql.Date(parsed.getTime());
         
                    
                    
                    
            PreparedStatement ps = DB.getInstance().getPreparedStatement("INSERT INTO students values(?,?,?,?);");
            ps.setString(1,id);     
            ps.setString(2,names);
            ps.setString(3,lastnames);
            ps.setDate(4,  birdth); 
            
            int rowsUpdated = ps.executeUpdate();
            System.out.println(rowsUpdated);
            }    catch (Exception e) {
                
                 e.printStackTrace();
            } 
            return "1";
        }
        return "0";
    }
}
