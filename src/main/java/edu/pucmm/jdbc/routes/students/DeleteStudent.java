package edu.pucmm.jdbc.routes.students;
import edu.pucmm.jdbc.models.RouteBase;
import edu.pucmm.jdbc.utils.DB;
import edu.pucmm.jdbc.models.RouteBase;
import java.sql.PreparedStatement;
import spark.Request;
import spark.Response;

/**
 * @author a.marte
 */
public class DeleteStudent extends RouteBase {

    public DeleteStudent() {
        super("/students/delete");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        String idstr = request.queryParams("id");
        
        System.out.println("borro todo="+idstr);
        
        if (idstr != null) {
             
 
try{
    
    int ID = Integer.parseInt(idstr);
    PreparedStatement ps = DB.getInstance().getPreparedStatement("DELETE FROM students where id=? ");
        ps.setInt(1, ID );
int rowsDeleted = ps.executeUpdate();
}    catch (Exception e) {
                
                 e.printStackTrace();
            } 
            return "1";
        }
        return "0";
    }
}
