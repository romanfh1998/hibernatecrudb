package edu.pucmm.jdbc.routes;

import edu.pucmm.jdbc.domains.Student;
import edu.pucmm.jdbc.models.RouteBase;
import edu.pucmm.jdbc.utils.DB;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import spark.Request;
import spark.Response;

/**
 * @author a.marte
 */
public class IndexRoute extends RouteBase {

    public IndexRoute() {
        super("/");
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.init();
        Template template = velocityEngine.getTemplate("html/index.html");
        VelocityContext context = new VelocityContext();
        List<Student> students = new ArrayList<>();
        Statement statement = DB.getInstance().getStatement();
        ResultSet resultSet = statement.executeQuery("SELECT id, names, lastnames, birthDate FROM students");
        while (resultSet.next()) {
            Student student = new Student();
            student.setId(resultSet.getInt("id"));
            student.setNames(resultSet.getString("names"));
            student.setLastnames(resultSet.getString("lastnames"));
            student.setBirthDate(resultSet.getDate("birthDate"));
            students.add(student);
        }
        context.put("students", students);
        StringWriter writer = new StringWriter();
        template.merge(context, writer);
        return writer.toString();
    }

}
